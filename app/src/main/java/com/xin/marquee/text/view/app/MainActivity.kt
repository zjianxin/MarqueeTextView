package com.xin.marquee.text.view.app

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.xin.marquee.text.view.MarqueeTextView
import com.xin.marquee.text.view.app.databinding.ActivityMainBinding
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "MainActivity"
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.edtContent.setSelection(binding.edtContent.text?.length ?: 0)
        val view = binding.tv1
        binding.btnContent.setOnClickListener {
            view.text = binding.edtContent.text.toString()
        }
        binding.btnStart.setOnClickListener {
            view.start()
        }
        binding.btnStop.setOnClickListener {
            view.stop()
        }
        binding.btnToggle.setOnClickListener {
            view.toggle()
        }
        binding.btnColor.setOnClickListener {
            val random = Random.Default
            view.textColor =
                Color.rgb(random.nextInt(0, 255), random.nextInt(0, 255), random.nextInt(0, 255))
        }
        binding.btnTextUp.setOnClickListener {
            view.textSize = view.textSize + 2
        }
        binding.btnTextDown.setOnClickListener {
            view.textSize = view.textSize - 2
        }
        binding.btnSpeedUp.setOnClickListener {
            view.speed = view.speed + 2
        }
        binding.btnSpeedDown.setOnClickListener {
            view.speed = view.speed - 2
        }
        binding.btnContinuous.setOnClickListener {
            view.repeat = MarqueeTextView.REPEAT_FILL_LOOP
        }
        binding.btnInterval.setOnClickListener {
            view.repeat = MarqueeTextView.REPEAT_SINGLE_LOOP
        }
        binding.btnOnce.setOnClickListener {
            view.repeat = MarqueeTextView.REPEAT_SINGLE
        }
    }

}