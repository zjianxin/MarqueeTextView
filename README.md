# MarqueeTextView [![](https://jitpack.io/v/com.gitee.zjianxin/MarqueeTextView.svg)](https://jitpack.io/#com.gitee.zjianxin/MarqueeTextView)

#### 介绍
Kotlin 自定义 View 实现文本横向滚动，跑马灯效果。`TextView` 自带的 `android:ellipsize="marquee"` 无法满足需求，而且焦点变化后还可能停止滚动。`  
![demo](demo.jpg "demo")
#### 安装教程
- 工程`build.gradle`添加 `maven { url 'https://jitpack.io' }`
```groovy
allprojects {
		repositories {
			//...
			maven { url 'https://jitpack.io' }
		}
}
```
- 模块`build.gradle`
```groovy
dependencies {
	 implementation 'com.gitee.zjianxin:MarqueeTextView:1.1'
}
```

#### 使用说明
- 参考 [MainActivity](app/src/main/java/com/xin/marquee/text/view/app/MainActivity.kt)
```xml
<com.xin.marquee.text.view.MarqueeTextView
    android:id="@+id/tv1"
    android:layout_width="match_parent"
    android:layout_height="50dp"
    android:layout_marginStart="20dp"
    android:layout_marginEnd="20dp"
    android:layout_marginBottom="16dp"
    android:text="Kotlin 实现文本横向滚动，跑马灯效果。"
    android:textSize="14dp"
    app:marqueeRepeat="fillLoop"
    app:marqueeSpeed="5" />
```
